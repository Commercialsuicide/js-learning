var generatePrimesArray = require('./generatePrimesArray');


function findPrimes(number) {
    var primesArray = generatePrimesArray(number);
    var primes = [];
    var num = number;

    for (var i = 0; i <= primesArray.length; i++) {
        while (num % primesArray[i] === 0) {
            num = num / primesArray[i];
            primes.push(primesArray[i]);
        }
    }
    console.log(primes);

}


findPrimes(781);