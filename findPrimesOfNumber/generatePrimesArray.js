function generatePrimesArray(limit) {

    var numbers = [2];
    for (var i = 3; i <= limit; i++) {
        var needPush = true;
        for (var j = 0; j < numbers.length; j++) {
            if (i % numbers[j] === 0) {
                needPush = false;
                break;
            }
        }

        if (needPush) {
            numbers.push(i);
        }
    }

    return numbers;

}

module.exports = generatePrimesArray;