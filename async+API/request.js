// 'npm init'
// install 'require'
// switch ON settings-languages-node.js

var request = require('request'); // 'request' - NOT DEFAULT module
var fs = require('fs'); // default module

function myCallback(err, response, body) { // 3 standard parameters
    if (err) {
        console.log('ERROR');
        return; // stops function
    }

    /*var obj = JSON.parse(body);

    console.log(obj);*/

    fs.writeFile('./async+API/finalFile.js', body, function (err) { // write STRING in file
        if (err) {
            return console.log(err);
        }

        fs.readFile('./async+API/finalFile.js', function (err, data) { // read STRING from file
            if (err) {
                return console.error(err);
            }
            console.log("Asynchronous read: " + data);
        });
    });
}

request('https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_NXT&depth=10', myCallback); // ('API, callbackFunction')