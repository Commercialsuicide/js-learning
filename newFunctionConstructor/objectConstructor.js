module.exports = UserConstructor;

function UserConstructor(name, lastname, year, month, day) {
    var self = this; // because use IIFE

    this.name = name;

    this.lastname = lastname;

    this.fullname = (function () {
        return name + ' ' + lastname;
    }());

    this.birthday = year + '/' + month + '/' + day;

    this.age = (function (dateString) {
        var today = new Date();
        var birthDate = new Date(dateString);
        var age = today.getFullYear() - birthDate.getFullYear();
        var m = today.getMonth() - birthDate.getMonth();
        if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
            age--;
        }
        return age;
    }(this.birthday));

    this.gang = (function () {
       if (self.fullname === 'Commercial Suicide' || self.fullname === 'Krab Radosty' || self.fullname === 'Jewish Content') {
            return '*** Sliv Zaschita ***';
        } else {
            return '* unknown *';
        }
    }());

    this.awesome = (function () {
        if (self.gang === '*** Sliv Zaschita ***') {
            return true;
        } else {
            return false;
        }
    }());
}