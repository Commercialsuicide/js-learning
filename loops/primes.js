var numbers = [2];
for (var i = 3; i <= 100; i++) {
    var needPush = true;
    for (var j = 0; j < numbers.length; j++) {
        if (i % numbers[j] === 0) {
            needPush = false;
            break;
        }
    }

    if (needPush) {
        numbers.push(i);
    }
}
console.log(numbers);

/*
 var numbers = [2];
 num:
 for (var i = 3; i <= 100; i++) {
 for (var j = 0; j < numbers.length; j++) {
 if (i % numbers[j] === 0) {
 continue num;
 }
 }

 numbers.push(i);
 }
 console.log(numbers);
 */