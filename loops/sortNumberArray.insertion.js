var numbers = [5, 3, 1, 2, 4];
for (var i = 0; i < numbers.length; i++) { // or "var i = 1" for faster iteration
    var copyI = numbers[i];
    for (var j = i - 1; j >= 0 && (numbers[j] > copyI); j--) { // or use "numbers[j] > copyI" as another "if" statement
        numbers[j+1] = numbers[j];
    }
    numbers[j+1] = copyI;
    console.log('process: ' + numbers);
}
console.log('*RESULT*: ' + numbers);
