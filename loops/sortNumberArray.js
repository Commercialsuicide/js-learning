/* The most fast way to sort an array */

var numArray = [2, 16, 7, 154, 30, 1, 18, 0, 99];
numArray = numArray.sort(function (a, b) {
    return a - b;
});
console.log('Ascending: ' + numArray);

numArray = numArray.sort(function (a, b) {
    return b - a;
});
console.log('Descending: ' + numArray);