var numbers = [5, 3, 1, 2, 4];
for (var i = 0; i < numbers.length - 1; i++) {
    var min = i;
    for (var j = i + 1; j < numbers.length; j++) {
        if (numbers[i] > numbers[j]) {
            var target = numbers[i];
            numbers[min] = numbers[j];
            numbers[j] = target;
        }
    }
}
console.log(numbers);

/* ANOTHER WAY

var numArray = [140000, 104, 99];
for (var i = 0; i < numArray.length - 1; i++) {
    var min = i;
    for (var j = i + 1; j < numArray.length; j++) {
        if (numArray[j] < numArray[min]) {
            min = j;
        }
    }
    if(min != i) {
        var target = numArray[i];
        numArray[i] = numArray[min];
        numArray[min] = target;
    }
}
console.log(numArray);

 */