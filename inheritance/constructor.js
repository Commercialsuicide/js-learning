var isDangerous = require('./isDangerous.function');


var fish = {
    habitat: 'water',
    stupid: true
};

fish.dangerous = isDangerous;

function Fish(name, predator) {
    this.name = name;
    this.predator = predator;
}

Fish.prototype = fish;





var shark = new Fish('Shark', true);

function printResult() {
    console.log('Name: ' + shark.name);
    console.log('Habitat: ' + shark.habitat);
    console.log('Stupid: ' + shark.stupid);
    console.log('Predator: ' + shark.predator);
    console.log('Dangerous: ' + shark.dangerous());
}
printResult();