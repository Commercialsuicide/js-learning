var voice = require('./function.js');

function Dog(name, color, breed) {
    this.name = name;
    this.color = color;
    this.breed = breed;
}

Dog.prototype.bark = voice; //.bark - adding new property to Dog constructor


var regi = new Dog('Regi', 'black', 'labrador');

console.log(regi.bark());