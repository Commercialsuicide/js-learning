var john = {
    name: 'John',
    lastname: 'Doe'
};

var jane = {
    name: 'Jane',
    lastname: 'Doe'
};


(function () {
    function isJohnDoe(obj) {
        var fullName = obj.name + ' ' + obj.lastname;

        if (fullName === 'John Doe') {
            console.log('Hi, ' + fullName + '!');
        } else {
            console.error('Hey, ' + fullName + ' , you\'re not John Doe!');
        }
    }

    isJohnDoe(jane);
}());