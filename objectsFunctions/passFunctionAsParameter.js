var john = {name: 'John', lastname: 'Doe'};
var jane = {name: 'Jane', lastname: 'Doe'};
var james = {name: 'James', lastname: 'Doe'};




function getFullName(obj) {
    return obj.name + ' ' + obj.lastname;
}


function getFullNameLength(obj) {
    var fullName = getFullName(obj);
    return fullName + ', Total length: ' + (fullName.length - 1);
}


function printResult(obj, fullNameFunc, lengthFunc) {
    var result = '';
    if (fullNameFunc) {
        result = fullNameFunc(obj);
    }

    if (lengthFunc) {
        result = lengthFunc(obj);
    }
    console.log(result);
}



printResult(james, getFullName, getFullNameLength);